// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#include "hand.h"
#include "hand_prop.h"
#include "glossary.h"
#include "exceptions.h"
#include "iowrapper.h"
#include "fiber_prop.h"
#include "fiber_grid.h"
#include "simul.h"
#include "sim.h"

//------------------------------------------------------------------------------

Hand::Hand(HandProp const* p, HandMonitor* m)
 : haNext(nullptr), haPrev(nullptr), haMonitor(m), prop(p)
{
    // initialize in unattached state:
    nextDetach = 0;
    nextAttach = RNG.exponential();
}


Hand::~Hand()
{
    // the Hands should be detached in ~Couple and ~Single
    assert_true(!fbFiber);
    prop = nullptr;
}


Hand * Hand::otherHand() const
{
    return haMonitor->otherHand(this);
}


Vector Hand::otherPosition() const
{
    return haMonitor->otherPosition(this);
}


real Hand::interactionStiffness() const
{
    return haMonitor->interactionStiffness();
}


void Hand::resetTimers()
{
    // initialize the Gillespie counters:
    if ( attached() )
    {
        nextDetach = RNG.exponential();
    }
    else
    {
        nextDetach = 0;
    }
}

//------------------------------------------------------------------------------
#pragma mark -

void Hand::relocate(Fiber* f)
{
    assert_true(f);
    if ( fbFiber )
    {
        fbFiber->removeHand(this);
        fbFiber = f;
#if FIBER_HAS_LATTICE
        if ( fbLattice )
            fbLattice = &f->lattice();
#endif
    }
    f->addHand(this);
    update();
}


void Hand::relocate(Fiber* f, const real a)
{
    assert_true(f);
    if ( f != fbFiber )
    {
        if ( fbFiber )
        {
            fbFiber->removeHand(this);
            fbFiber = f;
#if FIBER_HAS_LATTICE
            if ( fbLattice )
                fbLattice = &f->lattice();
#endif
        }
        f->addHand(this);
    }
    fbAbs = a;
    update();
}


void Hand::moveToEnd(const FiberEnd end)
{
    assert_true(fbFiber);
    assert_true(end==PLUS_END || end==MINUS_END);
    
    if ( end == PLUS_END )
        FiberSite::relocateP();
    else
        FiberSite::relocateM();
}

//------------------------------------------------------------------------------
#pragma mark -

/**
Checks that all the conditions required for attachment are met
 */
bool Hand::attachmentAllowed(FiberSite& sit) const
{
    assert_true( sit.attached() );
    
    /*
     Check that the two binding keys match, allowing binding 
     according to the BITWISE AND of the two keys:
     */
    if ( ! ( prop->binding_key & sit.fiber()->prop->binding_key ) )
        return false;
        
    // check end-on binding:
    if ( sit.abscissaFromM() < 0 )
    {
        if ( prop->bind_also_end & MINUS_END )
            sit.relocateM();
        else
            return false;
    }
    else if ( sit.abscissaFromP() < 0 )
    {
        if ( prop->bind_also_end & PLUS_END )
            sit.relocateP();
        else
            return false;
    }
    
    FiberEnd end = NO_END;

    switch ( prop->bind_only_end )
    {
        case NO_END:
            break;
        case MINUS_END:
            if ( sit.abscissaFromM() > prop->bind_end_range )
                return false;       // too far from fiber end
            end = MINUS_END;
            break;
        case PLUS_END:
            if ( sit.abscissaFromP() > prop->bind_end_range )
                return false;       // too far from fiber end
            end = PLUS_END;
            break;
        case BOTH_ENDS:
        {
            FiberEnd e = nearestEnd();
        
            if ( sit.abscissaFrom(e) > prop->bind_end_range )
                return false;
        
            // also check the other fiber end:
            FiberEnd o = ( e == PLUS_END ) ? MINUS_END : PLUS_END;
        
            // give equal chance to all ends within range:
            if ( sit.abscissaFrom(o) < prop->bind_end_range  )
                end = RNG.choice(MINUS_END, PLUS_END);
            else
                end = e;
        }
        default:
            throw Exception("Illegal value of hand:bind_only_end");
    }

#if NEW_BIND_ONLY_FREE_END
    // check occupancy near the end (should be done with FiberLattice)
    if ( end != NO_END && prop->bind_only_free_end )
    {
        if ( 0 < sit.fiber()->nbHandsNearEnd(prop->bind_end_range, end) )
            return false;
    }
#endif
    
    // also check the Monitor's permissions:
    return haMonitor->allowAttachment(sit);
}


void Hand::locate(Fiber* f, real a)
{
    assert_true(f);
    assert_true(!fbFiber);
    //assert_true(f->abscissaM() <= a + REAL_EPSILON);
    //assert_true(a <= f->abscissaP() + REAL_EPSILON);

    fbAbs   = a;
    fbFiber = f;
    f->addHand(this);
    update();
    haMonitor->afterAttachment(this);
    nextDetach = RNG.exponential();
}


void Hand::attach(FiberSite const& s)
{
    assert_true(s.attached());
    assert_true(!fbFiber);
    nextDetach = RNG.exponential();
    //std::clog << "bound" << std::endl;

    locate(s.fiber(), s.abscissa());
#if FIBER_HAS_LATTICE
    fbLattice = s.lattice();
    fbSite = s.site();
#endif
}


void Hand::detach()
{
    assert_true( attached() );
    haMonitor->beforeDetachment(this);
    nextAttach = RNG.exponential();
    fbFiber->removeHand(this);
    fbFiber = nullptr;
#if FIBER_HAS_LATTICE
    fbLattice = nullptr;
#endif
}


void Hand::detachHand()
{
    assert_true( attached() );
    fbFiber->removeHand(this);
    fbFiber = nullptr;
#if FIBER_HAS_LATTICE
    fbLattice = nullptr;
#endif
}

//------------------------------------------------------------------------------
#pragma mark -


void Hand::checkFiberRange()
{
    assert_true( attached() );
    
    if ( fbAbs < fbFiber->abscissaM() )
        handleDisassemblyM();
    else if ( fbAbs > fbFiber->abscissaP() )
        handleDisassemblyP();
}


void Hand::handleDisassemblyM()
{
    if ( RNG.test(prop->hold_shrinking_end) )
        relocateM();
    else
        detach();
}

void Hand::handleDisassemblyP()
{
    if ( RNG.test(prop->hold_shrinking_end) )
        relocateP();
    else
        detach();
}

//------------------------------------------------------------------------------
#pragma mark -

/**
 Test for attachment to nearby Fibers
 */
void Hand::stepUnattached(Simul& sim, Vector const & pos)
{
    assert_true( unattached() );
    assert_true( nextAttach >= 0);

/**
Enable NEW_GILLESPIE in sim.h to enable the original
Gillespie method for calculating binding event probabilities
if not enabled, binding will be calculated with the newer method
*/
#if NEW_GILLESPIE
    nextAttach -= prop->binding_rate_dt * 4;
    //std::clog << "nextAttach" << nextAttach << std::endl;

    while ( nextAttach < 0 )
    {
        /*
         For each test, the probability to attach is 1/4, since we provide
         here a test integer '1<<30' which is 4 times smaller than '1<<32',
         which pass always. 
         The configuration of the hands & filaments will not change,
         between sucessive calls tryToAttach() here, but this should happen rarely.
         */
        //std::clog << "trying" << std::endl;
        sim.fiberGrid.tryToAttach(pos, *this);
            
        if ( attached() )
            //std::clog << "attached" << std::endl;
            break;
        nextAttach += RNG.exponential();
    }

#else    

    sim.fiberGrid.tryToAttach(pos, *this);

#endif

}


/**
 Test for spontaneous detachment at rate HandProp::unbinding_rate, 
 */
void Hand::stepUnloaded()
{
    assert_true( attached() );
    
    testDetachment();
}

/**
 Test for force-induced detachment following Kramers' law,
 vith basal rate HandProp::unbinding_rate, 
 and characteristic force HandProp::unbinding_force
 */
void Hand::stepLoaded(Vector const& force, real force_norm)
{
    assert_true( attached() );
    assert_true( nextDetach >= 0 );
    
    if ( prop->unbinding_force_inv > 0 )
    {
/**
enable NEW_UNBIND in hand.h to allow for Detachment calculations using
the Kramers method, a catch-slip method (Stam et al. 2015), and a catch
method (Erdmann et al. 2013)
*/
#if NEW_UNBIND
        testVariableDetachment(force_norm);
#else
        testKramersDetachment(force_norm);
#endif
    }
    else
        testDetachment();
}

bool Hand::testVariableDetachment(const real force)
{
    /*
     mul can be infinite, because it is the exponential of force,
     and we avoid the case ( rate==0 ) since zero * infinite is undefined
     */
    if ( prop->unbinding_rate_dt > 0 )
    {
        //detach_mode denotes whether unbinding should be estimated by Kramers (0), Guo(1), or Erdmann (2) theory
        if (prop->detach_mode == "Slip")
        {
            //Kramers Theory calculation of unbinding rate
            d_rate = prop->unbinding_rate_dt * exp(force * prop->unbinding_force_inv);
            nextDetach -= d_rate;
            //d_rate *= 1000;
            //std::clog << "force," << force << std::endl;
            //std::clog << "d_rate," << d_rate << std::endl;

            if ( nextDetach <= 0 )
            {
                detach();
                return true;
            }
        }

        if ( prop->detach_mode == "CatchSlip")
        {
            //unbinding_constants = bond length (nm) / kT (pNnm); these are multiplied by input force from Cytosim to 
            //generate dimensionless descriptors of unbinding. Equation is described in Guo et al. 2006 and Stam et al 2015
            d_rate = ((prop->unbinding_rate_dt) * ((prop->alpha_catch * exp(-1 * force * prop->x_catch)) + (prop->alpha_slip * exp(force * prop->x_slip))));
            nextDetach -= d_rate;
            //d_rate *= 1000;
            //std::clog << "force," << force << std::endl;
            //std::clog << "d_rate," << d_rate << std::endl;

            if ( nextDetach <= 0 )
            {
                detach();
                return true;
            }
        }

        if ( prop->detach_mode == "Catch")
        {
            //unbinding_constants = bond length (nm) / kT (pNnm); these are multiplied by input force from Cytosim to 
            //generate dimensionless descriptors of unbinding. Equation is described in Guo et al. 2006 and Stam et al 2015
            d_rate = ((prop->unbinding_rate_dt) *  (prop->alpha_catch * exp(-1 * force * prop->x_catch)));
            nextDetach -= d_rate;
            //d_rate *= 1000;
            //std::clog << "force," << force << std::endl;
            //std::clog << "d_rate," << d_rate << std::endl;

            if ( nextDetach <= 0 )
            {
                detach();
                return true;
            }
        }
    
        //Calculates the detachment time (T10) of ensembles of size Nt based on Erdmann 2016
        if (prop->detach_mode == "PCM")
        {
            //intergers used for calculating bound motors (i) and post-power stroke (j)
            int i = 0;
            int j = 0;
            int k = 0;
            const int Nt = prop->ensemble_size; //number of motors per side of minifilament
            const int States = Nt + 1;  //Index size for arrays including 0<=i<=Nt

            const int aSize = States * States;  //generates static arrays for calculations

           
            real Km = 2.5; //motor elasticity range in Erdmann, value from Stam
            real d = 8; //power stroke distance from Stam et al
            real Epp = -60; //Post-power-stroke bias from Erdmann
            real BT = 4.14; //KbT in pN nm
            real kzero20 = prop->k_20; //off-rate post-power stroke F0
            real F0 = 12.62; //F0 unbinding force from Erdmann
            real k10 = prop->k_10; //off-rate pre-power stroke
            real k01 = prop->k_01; //on-rate 
            real delta = 0.328; //Unbinding distance Erdmann

            //DT is equivalent to sim->prop->time_step
            real DT = prop->binding_rate_dt / prop->binding_rate / prop->ensemble_size; 

            real x[aSize] = {0}; // Xij for Eq. 4
            real Eel[aSize] = {0}; // Eel|ij for Eq. 7
            real E[aSize] = {0}; //Eij for Eq. 6
            real Z[aSize] = {0}; //Zi for Eq. 6
            real pji[aSize] = {0}; //p(j|i) for Eq. 5
            real F[aSize] = {0}; //Fij for Eq. 11
            real k20[aSize] = {0}; //Effective K20 for Eq. 11
            real r[aSize] = {0};  //r(i,j) for Eq. 12

            real ri[States] = {0}; //r(i) for Eq. 13
            real gi[States] = {0}; //g(i) for Eq. 14
            real Zi[States] = {0}; //sum of all Zi for Eq. 6

            real T10prod[aSize] = {0}; //PI operator for Eq. 44
            real T10_PROD[States] = {0}; //all products of PI for Eq. 44
            real T10_SUM[States] = {0};
            real T10;

            //zero values; probably not necessary
            pji[0] = 0.0;
            x[0] = 0.0;
            Eel[0] = 0.0;
            Z[0] = 1.0;
            E[0] = 0.0;
            Zi[0] = 1.0;
            gi[0] = Nt * k01;
               
            for ( i = 1; i < States; i++)
            {
                gi[i] = (Nt - i) * k01;
                //std::clog << " hand::Fext(" << force << ")" << std::endl;
            }

            for ( i = 1; i < States; i++)
            {
                for (j = 0; j < i + 1; j++)
                {
                    x[j+(States*i)] = ((force / Km) - (j * d)) / i;
                    Eel[j+(States*i)] = (Km / 2) * ((( i - j) * (square(x[j+(States*i)]))) + (j * square(x[j+(States*i)] + d)));
                    E[j+(States*i)]  = j * Epp + Eel[j+(States*i)];
                    Z[j+(States*i)] = exp(-E[j+(States*i)] / BT);
                    

                    if (std::isinf(Z[j+(States*i)]))
                    {
                        std::clog << " Too big Z!" << std::endl;
                        Z[j+(States*i)] = 1e+100;
                    }

                    F[j+(States*i)] = Km * (x[j+(States*i)] + d);
                    k20[j+(States*i)] = kzero20 * exp(-F[j+(States*i)] / F0);
                    r[j+(States*i)] = (i - j) * k10 + ( j * k20[j+(States*i)]);
                    Zi[i] = Zi[i] + Z[j+(States*i)];
                    //std::clog << " hand::Zi(" << Zi[i] << ")" << std::endl;
                }
            }

            for ( i = 1; i < States; i++)
            {
                for (j = 0; j < i + 1; j++)
                {
                    x[j+(States*i)] = ((force / Km) - (j * d)) / i;
                    pji[j+(States*i)] = Z[j+(States*i)] / Zi[i];
                    ri[i] = ri[i] + (r[j+(States*i)] * pji[j+(States*i)]);

                }

            } 

            for (j = 1; j < States; j++)
            {
                T10_PROD[j] = 1;
                for (k = 1; k < j; k++)
                {
                    T10prod[k+(States*j)] = gi[k] / ri[k];
                    T10_PROD[j] *= T10prod[k+(States*j)];
                }
                T10_SUM[j] = (1 / ri[j]) * T10_PROD[j]; 
                T10_PROD[j] = 1;
                T10 += T10_SUM[j];
                //std::clog << " hand::T10(" << T10 << ")" << std::endl;
            }


            //These limits may not be necessary for ensembles less than 30 motors       
            //if (T10 > 1 * exp(100))  //set upper limit for real
            //{
            //  std::clog << " Too big Z!" << std::endl;
            //    T10 = 1 * exp(100);
            //}

            //if (T10 < 1 * exp(-100))
            //{
            //  std::clog << " Too small Z!" << std::endl;
             //   T10 = 1 * exp(-100); //set lower limit for real
            //}

            //if (std::isnan(T10))
            //{
           //   std::clog << " Too small Z!" << std::endl;
            //    T10 = 1 * exp(-100);  //isNan = lower limit
            //}

            //if(std::isinf(T10))
            //{
            //  std::clog << " Too big Z!" << std::endl;
            //    T10 = 1 * exp(100); //isInf = upper limit
            //}
            T10 = (1.0/ T10) * DT;
            nextDetach -= T10;
            //std::clog << "force," << force << std::endl;
            //std::clog << "T10," << T10 << std::endl;

            if ( nextDetach <= 0 )
            {
                detach();
                return true;
            }
        }
    }
    return false;
}

//Used to calculate the estimated speed of bound motors as described in Erdman 2016
//TO DO: define PI (product) function, use square() instead of pow()
real Hand::meanVel(const real force) const
{
            int i = 0;
            int j = 0;
            int k = 0;
            const int Nt = prop->ensemble_size; //number of motors per side of minifilament
            const int States = Nt + 1;  //Index size for arrays including 0<=i<=Nt

            const int aSize = States * States;  //generates static arrays for calculations

           
            real Km = 2.5; //motor elasticity range in Erdmann, value from Stam
            real d = 8; //power stroke distance from Stam et al
            real Epp = -60; //Post-power-stroke bias from Erdmann
            real BT = 4.14; //KbT in pN nm
            real kzero20 = prop->k_20; //off-rate post-power stroke F0
            real F0 = 12.62; //F0 unbinding force from Erdmann
            real k10 = prop->k_10; //off-rate pre-power stroke
            real k01 = prop->k_01; //on-rate 
            real delta = 0.328; //Unbinding distance Erdmann

            //DT is equivalent to sim->prop->time_step
            real DT = prop->binding_rate_dt / prop->binding_rate / prop->ensemble_size;

            real x[aSize] = {0}; // Xij for Eq. 4
            real Eel[aSize] = {0}; // Eel|ij for Eq. 7
            real E[aSize] = {0}; //Eij for Eq. 6
            real Z[aSize] = {0}; //Zi for Eq. 6
            real pji[aSize] = {0}; //p(j|i) for Eq. 5
            real F[aSize] = {0}; //Fij for Eq. 11
            real k20[aSize] = {0}; //Effective K20 for Eq. 11
            real r[aSize] = {0};  //r(i,j) for Eq. 12
            real zON[aSize] = {0}; //zon|ij for Eq. 26
            real ZON[aSize] = {0}; //Zi|on for Eq. 32

            real ri[States] = {0}; //r(i) for Eq. 13
            real vi[States] = {0}; // vi for Eq. 33
            real gi[States] = {0}; //g(i) for Eq. 14
            real Zi[States] = {0}; //sum of all Zi for Eq. 6

            real ZiON[States] = {0}; //Zi|on for Eq. 32

            real p[States] = {0}; //probability of i bound motors
            real p_hat[States] = {0}; //probability of i bound motors (ignore i=0)
            real TOP[aSize] = {0}; //top PI operator for Eq. 41
            real BOTTOM[aSize] = {0}; //bottom PI operator for Eq. 41
            real TOP_PROD[States] = {1}; //sum of all prod1
            real BOTTOM_PROD[States] = {1}; //sum of all prod2
            real SUMp = 0; //sum of all pi
            real vb = 0; //total bound velocity
            real cvb = 0; //vb_dt
            real i1state = 0; //unbinding rates when (1,0) and (1,1)

            //zero values; probably not necessary
            pji[0] = 0.0;
            x[0] = 0.0;
            Eel[0] = 0.0;
            Z[0] = 1.0;
            E[0] = 0.0;
            Zi[0] = 1.0;
            vi[0] = 0.0; 
            gi[0] = Nt * k01;
               
            for ( i = 1; i < States; i++)
            {
                gi[i] = (Nt - i) * k01;
            
            }

            for ( i = 1; i < States; i++)
            {
                for (j = 0; j < i + 1; j++)
                {
                    x[j+(States*i)] = ((force / Km) - (j * d)) / i;
                    Eel[j+(States*i)] = (Km / 2) * ((( i - j) * (square(x[j+(States*i)]))) + (j * square(x[j+(States*i)] + d)));
                    E[j+(States*i)]  = j * Epp + Eel[j+(States*i)];
                    Z[j+(States*i)] = exp(-E[j+(States*i)] / BT);
                    
                    if (std::isinf(Z[j+(States*i)]))
                    {
                       Z[j+(States*i)] = 1e+100;
                    }

                    F[j+(States*i)] = Km * (x[j+(States*i)] + d);
                    k20[j+(States*i)] = kzero20 * exp(-F[j+(States*i)] / F0);
                    r[j+(States*i)] = (i - j) * k10 + ( j * k20[j+(States*i)]);
                    Zi[i] = Zi[i] + Z[j+(States*i)];
                }
            }

            for ( i = 1; i < States; i++)
            {
                for (j = 0; j < i + 1; j++)
                {
                    x[j+(States*i)] = ((force / Km) - (j * d)) / i;
                    pji[j+(States*i)] = Z[j+(States*i)] / Zi[i];
                    ri[i] = ri[i] + (r[j+(States*i)] * pji[j+(States*i)]);
                    zON[j+(States*i)] = -1 * ((x[j+(States*i)]) / (i + 1));
                    ZON[j+(States*i)] = zON[j+(States*i)] * pji[j+(States*i)];
                    ZiON[i] += ZON[j+(States*i)];
                }

                i1state = (k10 * x[States] * pji[States]) + (k20[1+ (States)] * x[1+ (States)] * pji[1+ (States)]);
                vi[i] = (gi[i] * ZiON[i]) - (i1state *delta); 
            } 

            for (i = 1; i < States; i++)
            {
                gi[0] = Nt * k01;
                TOP_PROD[i] = 1;

                for (j = 0; j < i; j++)
                {
                    
                    TOP[j+(States*i)] = gi[j] / ri[j+1];
                    TOP_PROD[i] *= TOP[j+(States*i)];
                }

            }

            for (k = 1; k < States; k++)
            {
                gi[0] = Nt * k01;
                BOTTOM_PROD[k] = 1;
                for (j = 0; j < k; j++)
                {
                    BOTTOM[j+(States*k)] = gi[j] / ri[j+1];
                    BOTTOM_PROD[k] *= BOTTOM[j+(States*k)];
                }
                SUMp += BOTTOM_PROD[k];
            }

            for (i = 1; i < States; i++)
            {
                p_hat[i] = TOP_PROD[i] / (SUMp);
                vb += vi[i] * p_hat[i];
            }

            vb = vb * 0.001; //Erdmann measures in? [need to figure out if this conversion is correct]
            cvb = vb * DT;
            //std::clog << " force:(" << force << ")" << std::endl;
            //std::clog << " hand::bound velocity(" << cvb << ")" << std::endl;
    return cvb;

}
//------------------------------------------------------------------------------
#pragma mark -


void Hand::write(Outputter& out) const
{
    /*
     it is not necessary to write the property number here,
     since it is set when the Hand is created in class Single or Couple.
     */
    FiberSite::write(out);
}


void Hand::read(Inputter& in, Simul& sim)
{
#ifdef BACKWARD_COMPATIBILITY
    if ( in.formatID() < 32 )
        prop = sim.findProperty<HandProp>("hand",in.readUInt16());
#endif
    
    Fiber * fib = fbFiber;
    FiberSite::read(in, sim);

    // initialize the Gillespie counters:
    if ( attached() )
        nextDetach = RNG.exponential();
    else
        nextAttach = RNG.exponential();
    resetTimers();
    
    // update fiber's lists:
    if ( fib != fbFiber )
    {
        if ( fib )
            fib->removeHand(this);
        if ( fbFiber )
            fbFiber->addHand(this);
    }
}

std::ostream& operator << (std::ostream& os, Hand const& obj)
{
    os << "hand(" << obj.fiber()->reference() << ", " << obj.abscissa() << ")";
    return os;
}
