// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
#include "space_dynamic_ring.h"
#include "exceptions.h"
#include "mecapoint.h"
#include "iowrapper.h"
#include "glossary.h"
#include "meca.h"
#include "dim.h"


SpaceDynamicRing::SpaceDynamicRing(SpaceProp const* p)
: Space(p)
{
    if ( DIM < 3 )
        throw InvalidParameter("ring is only valid in 3D: use rectangle instead");
    length_ = 0;
    radius_ = 0;
}


void SpaceDynamicRing::resize(Glossary& opt)
{
    real len = length_, rad = radius_;
    
    if ( opt.set(rad, "diameter") )
        rad *= 0.5;
    else opt.set(rad, "radius");
    if ( opt.set(len, "length") )
        len *= 0.5;

    if ( len < 0 )
        throw InvalidParameter("ring:length must be > 0");
    if ( rad < 0 )
        throw InvalidParameter("ring:radius must be >= 0");

    length_ = len;
    radius_ = rad;
    update();
}


void SpaceDynamicRing::boundaries(Vector& inf, Vector& sup) const
{
    inf.set(-length_,-radius_,-radius_);
    sup.set( length_, radius_, radius_);
}


real  SpaceDynamicRing::volume() const
{
    return 2 * M_PI * length_ * radius_ * radius_;
}


Vector SpaceDynamicRing::randomPlace() const
{
#if ( DIM >= 3 )
    const Vector2 V = Vector2::randB(radius_);
    return Vector(length_*RNG.sreal(), V.XX, V.YY);
#elif ( DIM > 1 )
    return Vector(length_*RNG.sreal(), radius_*RNG.sreal());
#else
    return Vector(length_*RNG.sreal());
#endif
}


//------------------------------------------------------------------------------
bool  SpaceDynamicRing::inside(Vector const& w) const
{
#if ( DIM > 2 )
    const real RT = w.YY * w.YY + w.ZZ * w.ZZ;
    return ( fabs(w.XX) <= length_  &&  RT <= radiusSqr_ );
#else
    return false;
#endif
}

bool  SpaceDynamicRing::allInside(Vector const& w, const real rad ) const
{
    assert_true( rad >= 0 );

#if ( DIM > 2 )
    const real RT = w.YY * w.YY + w.ZZ * w.ZZ;
    return ( fabs(w.XX) + rad <= length_  &&  RT <= square(radius_-rad) );
#else
    return false;
#endif
}

//------------------------------------------------------------------------------
/**
 Project always on the surface of the cylinder
 */
Vector SpaceDynamicRing::project(Vector const& w) const
{
    Vector p;
    if ( w.XX >  length_ )
        p.XX =  length_;
    else if ( w.XX < -length_ )
        p.XX = -length_;
    else
        p.XX = w.XX;
    
#if ( DIM > 2 )
    real n = sqrt( w.YY*w.YY+ w.ZZ*w.ZZ );
    
    if ( n > 0 )
    {
        n = radius_ / n;
        p.YY = n * w.YY;
        p.ZZ = n * w.ZZ;
    }
    else
    {
        p.YY = radius_;
        p.ZZ = 0;
    }
#endif
    return p;
}

//------------------------------------------------------------------------------


/// add interactions to a Meca
void SpaceDynamicRing::setInteractions(Meca &, FiberSet const&) const
{
    force_ = 0;
}

/**
 This applies a force directed to the surface of the cylinder
 */
void SpaceDynamicRing::setInteraction(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff, const real len, const real rad)
{
    const index_t inx = DIM * pe.matIndex();

    if ( pos.XX > len )
    {
        meca.mC(inx, inx) -= stiff;
        meca.base(inx)    += stiff * len;
    }
    else if ( pos.XX < -len )
    {
        meca.mC(inx, inx) -= stiff;
        meca.base(inx)    -= stiff * len;
    }
    
    meca.addCylinderClampX(pe, rad, stiff);
}


/**
 This applies a force directed to the surface of the cylinder
 */
void SpaceDynamicRing::setInteraction(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
    meca.addSphereClamp(pos, pe, Vector(0,0,0), radius_, stiff);
    force_ += stiff * ( pos.norm() - radius_ );
    setInteraction(pos, pe, meca, stiff, length_, radius_);
}

/**
 This applies a force directed to the surface of the cylinder
 */
void SpaceDynamicRing::setInteraction(Vector const& pos, Mecapoint const& pe, real rad, Meca & meca, real stiff) const
{
    if ( radius_ > rad )
    {
        meca.addSphereClamp(pos, pe, Vector(0,0,0), radius_-rad, stiff);
        force_ += stiff * ( rad + pos.norm() - radius_ );
    }
    else {
        meca.addPointClamp( pe, Vector(0,0,0), stiff );
        std::cerr << "object is too big to fit in SpaceDisc\n";
        force_ += 2 * stiff * ( rad - radius_ );
    }

    real eRadius = radius_ - rad;
    if ( eRadius < 0 ) eRadius = 0;
    real eLength = length_ - rad;
    if ( eLength < 0 ) eLength = 0;
    setInteraction(pos, pe, meca, stiff, eLength, eRadius);
}

void SpaceDynamicRing::step()
{
    if (prop->resistance == "sphere")
    {
        real X1 = 0.00092680;
        real X2 = -0.09856350;
        real X3 = 0.59957168;
        real X4 = 10.51203388;
        real X5 = -1.06370229;

       Fr = prop->tension * ((((X1 * radius_ + X2) * radius_ + X3) * radius_ + X4) * radius_ + X5);
    }
   
    if (prop->resistance == "cell")
    {
        real X1 = -0.00051623;
        real X2 = 0.001031165;
        real X3 = 0.1652816;
        real X4 = -2.254861338;
        real X5 = 18.98429387;
        real X6 = 4.401933039;

        Fr = prop->tension * (((((X1 * radius_+ X2) * radius_+ X3) * radius_+ X4) * radius_+ X5) * radius_+ X6);
    }
    
    if (prop->resistance == "cylinder")
    {
        real X1 = -0.0013113329184;
        real X2 = 0.03584650124;
        real X3 = -0.3848857069;
        real X4 = 1.400795097;
        real X5 = 10.54636113;
        real X6 = 6.58934065;

        Fr = prop->tension * (((((X1 * radius_+ X2) * radius_+ X3) * radius_+ X4) * radius_+ X5) * radius_+ X6);
        if(radius_== rzero)
        {
            Fr = 0;
        }
    }

    if (prop-> resistance == "constant")
    {
        real X1 = -0.0013113329184;
        real X2 = 0.03584650124;
        real X3 = -0.3848857069;
        real X4 = 1.400795097;
        real X5 = 10.54636113;
        real X6 = 6.58934065;

        Fr = prop->tension * (((((X1 * rzero + X2) * rzero + X3) * rzero + X4) * rzero + X5) * rzero + X6);
    }

    else if (prop->resistance == "pombe")
    {
        real X1 = -13.113329184;
        real X2 = 35.84650124;
        real X3 = -38.48857069;
        real X4 = 14.00795097;
        real X5 = 10.54636113;
        real X6 = 0.658934065;

        Fr = prop->tension * (((((X1 * radius_+ X2) * radius_+ X3) * radius_+ X4) * radius_+ X5) * radius_+ X6);
    }

    if ( Fr < 0 && radius_== rzero )
        Fr = 0;

    Fr = abs(Fr);

    force_ = abs(force_);

    real dr = prop->mobility_dt * (force_ - Fr);
    //std::clog << "SpaceDisc:  radius_" << std::setw(12) << radius_<< " force =" << rForce << " dradius_=" << dr << " resistive_force =" << Rf << " expand ="<< expanse <<  " resistance =" << prop->resistance << "\n";
    //std::clog << "RingForce: " << force_ << "  Resistance: " << Fr << "  dr: " << dr << "\n";
    radius_ += dr;
}

real SpaceDynamicRing::getRadius()
{
    //std::clog << "SpaceDisc:  radius " << radius_ << "\n'";
    return radius_;
}


//------------------------------------------------------------------------------

void SpaceDynamicRing::write(Outputter& out) const
{
    out.put_characters("ring", 16);
    out.writeUInt16(2);
    out.writeFloat(length_);
    out.writeFloat(radius_);
}


void SpaceDynamicRing::setLengths(const real len[])
{
    length_ = len[0];
    radius_ = len[1];
    update();
}


void SpaceDynamicRing::read(Inputter& in, Simul&, ObjectTag)
{
    real len[8] = { 0 };
    read_data(in, len, "ring");
    setLengths(len);
}

//------------------------------------------------------------------------------
//                         OPENGL  DISPLAY
//------------------------------------------------------------------------------

#ifdef DISPLAY
#include "opengl.h"
#include "gle.h"

bool SpaceDynamicRing::draw() const
{
#if ( DIM > 2 )

    const size_t fin = 512;
    GLfloat c[fin+1], s[fin+1];
    gle::circle(fin, c, s, GLfloat(radius_));

    GLfloat L = GLfloat(length_);
    
    glBegin(GL_TRIANGLE_STRIP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        glNormal3f( 0, c[n], s[n]);
        glVertex3f(+L, c[n], s[n]);
        glVertex3f(-L, c[n], s[n]);
    }
    glEnd();
    
#endif
    return true;
}

#else

bool SpaceDynamicRing::draw() const
{
    return false;
}

#endif

